what does it do?
laptop_receipt is used over the summer when technology staff are not on location. When a student/parent dropps off a laptop the transaction is logged and a receipt is sent to an email address.

skills used:
1. gsheets
2. javascript

requirements:
1. web browser (tested in chrome)

how to run:
1. registrar enters a new student
2. when the student meets the requirements an email is sent to me so that I know to setup a new computer
