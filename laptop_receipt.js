function onEdit() {
  // set sheet variables
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getActiveSheet();
  var row = sheet.getLastRow();
  
  // set information variables
  var student_name = sheet.getRange(row, 3).getValue();
  var laptop_number = sheet.getRange(row, 4).getValue();
  var charger_number = sheet.getRange(row, 5).getValue();
  var send_to = sheet.getRange(row, 5).getValue();

  // compile email message
  var message = student_name.concat("\nLaptop Number = ", laptop_number,
                                    "\nCharger Returned = ", charger_number);   
  
  // check if email has already been sent
  var email_already_sent = sheet.getRange(row, 7).getValue();
  // send email
  if (email_already_sent != "email sent") {
    sendMail(send_to, message);
    sheet.getRange(row, 7).setValue("email sent");
  }

  // email function
  function sendMail(send_to, student_name) {
    var send_to = sheet.getRange(row, 6).getValue();
    MailApp.sendEmail(send_to, "HCPS Laptop/Charger Receipt", student_name);
  }
}
